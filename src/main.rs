extern crate ocl;

use ocl::{Image, Kernel, ProQue};

static KERNEL_SRC: &'static str = r#"
__kernel void img_fun(image2d_t a) {
}
"#;

fn main() {
    let pq = ProQue::builder()
        .src(KERNEL_SRC)
        .dims(1)
        .build()
        .expect("ProQue builder");
    let kern = pq.kernel_builder("img_fun")
        .arg(None::<&Image<u8>>)
        .build()
        .expect("Kernel builder");
}
